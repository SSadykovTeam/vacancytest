## Запуск

Сервис запустится на порту 8080(http://localhost:8080)

1. `docker-compose up -d --build site`
2. `docker-compose run --rm composer update`
3. `docker-compose exec php php artisan migrate`
4. `docker-compose exec php php artisan db:seed`

## Задача
Дано: 

Project
-title string
-description text

ProjectArticle

-title string
-content text


ProjectUser
-headline string
-first_name string


ProjectHasContent
-project_id
-? 


Отношения между объектами: 

ONE Project HAS MANY ProjectArticle
ONE Project HAS MANY ProjectUser

Написать actions для добавление файлов для ProjectArticle и ProjectUser используя библиотеку https://github.com/spatie/laravel-medialibrary

Написать добавление ProjectUser и ProjectArticle для Project используя полиморфные связи и промежуточную таблицу ProjectHasContent

Написать экшн для получения Project по :id вместе со всеми его ProjectArticle и ProjectUser, экшн должен возвращаться JSON и должен использовать

трасформеры Laravel (Illuminate\Http\Resources\Json\JsonResource класс)

Решение оформить в качестве репозитория с небольшой инструкцией