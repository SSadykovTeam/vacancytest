<?php

use App\Http\Controllers\ProjectArticleController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ProjectController;
use App\Http\Controllers\ProjectUserController;

Route::resource('projects', ProjectController::class)->only(['index', 'update', 'show']);
Route::resource('project-users', ProjectUserController::class)->only(['index', 'update']);
Route::resource('project-articles', ProjectArticleController::class)->only(['index', 'update']);
