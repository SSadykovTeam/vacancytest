<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphToMany;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;

/**
 * @property string $headline
 * @property string $first_name
 */

class ProjectUser extends Model implements HasMedia
{
    use HasFactory, InteractsWithMedia;

    protected $table = 'project_user';
    protected $fillable = ['headline', 'first_name'];
    protected $visible = ['id', 'headline', 'first_name'];

    public function project(): MorphToMany
    {
        return $this->morphToMany(Project::class, 'projectable', 'project_has_content');
    }
}
