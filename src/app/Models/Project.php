<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphToMany;
use Illuminate\Database\Eloquent\Collection as EloquentCollection;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * @property string $title
 * @property string $description
 * @property EloquentCollection $projectUsers
 * @property EloquentCollection $projectArticles
 */
class Project extends Model
{
    use HasFactory;

    protected $fillable = ['title', 'description'];
    protected $visible = ['id', 'title', 'description'];

    public function projectUsers(): MorphToMany
    {
        return $this->morphedByMany(ProjectUser::class, 'projectable', 'project_has_content');
    }

    public function projectArticles(): MorphToMany
    {
        return $this->morphedByMany(ProjectArticle::class, 'projectable', 'project_has_content');
    }
}
