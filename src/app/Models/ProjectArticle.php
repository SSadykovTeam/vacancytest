<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphToMany;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;

/**
 * @property string $title
 * @property string $content
 */
class ProjectArticle extends Model implements HasMedia
{
    use HasFactory, InteractsWithMedia;

    protected $table = 'project_article';
    protected $fillable = ['title', 'content'];
    protected $visible = ['id', 'title', 'content'];

    public function project(): MorphToMany
    {
        return $this->morphToMany(Project::class, 'projectable', 'project_has_content');
    }
}
