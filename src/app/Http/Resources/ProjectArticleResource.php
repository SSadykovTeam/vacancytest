<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Spatie\MediaLibrary\MediaCollections\Models\Media;
use App\Models\ProjectArticle;

class ProjectArticleResource extends JsonResource
{
    /** @var ProjectArticle */
    public $resource;

    public function toArray($request)
    {
        return [
            'images' => $this->resource->media->map(fn(Media $media) => $media->getUrl()),
        ] + parent::toArray($request);
    }
}
