<?php

namespace App\Http\Resources;

use App\Models\Project;
use Illuminate\Http\Resources\Json\JsonResource;

class ProjectResource extends JsonResource
{
    /** @var Project */
    public $resource;

    public function toArray($request)
    {
        return [
            'projectArticles' => blank($this->resource->projectArticles) ? [] : ProjectArticleResource::collection($this->resource->projectArticles),
            'projectUsers'    => blank($this->resource->projectUsers) ? [] : ProjectUserResource::collection($this->resource->projectUsers)
        ] + parent::toArray($request);
    }
}
