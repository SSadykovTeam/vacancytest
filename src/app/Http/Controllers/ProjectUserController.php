<?php

namespace App\Http\Controllers;

use App\Http\Resources\ProjectUserResource;
use App\Models\ProjectUser;
use Illuminate\Http\Request;

class ProjectUserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return ProjectUserResource::collection(ProjectUser::all());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\ProjectUser  $ProjectUser
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ProjectUser $projectUser)
    {
        if ($request->hasFile('image')) {
            $projectUser->addMediaFromRequest('image')->toMediaCollection();
        }

        $saveResult = $projectUser->fill($request->only(['headline', 'first_name']))->save();

        return response()->json([
            'status' => $saveResult,
            'ProjectUser' => new ProjectUserResource($projectUser),
        ]);
    }
}
