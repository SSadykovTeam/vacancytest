<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProjectUpdateRequest;
use App\Http\Resources\ProjectResource;
use App\Models\Project;
use App\Models\ProjectArticle;
use App\Models\ProjectUser;
use Illuminate\Http\Request;

class ProjectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return ProjectResource::collection(Project::all());
    }


    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function show(Project $project)
    {
        $project->loadMissing([
            'projectUsers',
            'projectArticles'
        ]);
        
        ProjectResource::withoutWrapping();
        return new ProjectResource($project);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function update(ProjectUpdateRequest $request, Project $project)
    {
        $saveResult = $project->fill($request->only(['title', 'description']))->save();

        $projectUsers = transform(
            $request->input('project_users'),
            fn (array $projectUsers) => collect($projectUsers)->map(fn (array $projectUser) => new ProjectUser($projectUser))
        );

        $projectArticles = transform(
            $request->input('project_articles'),
            fn (array $projectArticles) => collect($projectArticles)->map(fn (array $projectArticle) => new ProjectArticle($projectArticle))
        );

        if ($projectUsers) {
            $project->projectUsers()->saveMany($projectUsers);
        }

        if ($projectArticles) {
            $project->projectArticles()->saveMany($projectArticles);
        }

        return response()->json([
            'status' => $saveResult,
            'project' => new ProjectResource($project)
        ]);
    }
}
