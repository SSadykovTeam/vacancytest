<?php

namespace App\Http\Controllers;

use App\Http\Resources\ProjectArticleResource;
use App\Models\ProjectArticle;
use Illuminate\Http\Request;

class ProjectArticleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return ProjectArticleResource::collection(ProjectArticle::all());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\ProjectArticle  $projectArticle
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ProjectArticle $projectArticle)
    {
        if ($request->hasFile('image')) {
            $projectArticle->addMediaFromRequest('image')->toMediaCollection();
        }
        
        $saveResult = $projectArticle->fill($request->only(['title', 'content']))->save();

        return response()->json([
            'status' => $saveResult,
            'projectArticle' => new ProjectArticleResource($projectArticle),
        ]);
    }
}
