<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProjectUpdateRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'title'                       => 'required|string:max:191',
            'description'                 => 'string:max:2048',
            'project_users'               => 'array',
            'project_users.*.headline'    => 'required|string:max:191',
            'project_articles'            => 'array',
            'project_articles.*.title'    => 'required|string:max:191',
        ];
    }
}
