<?php

namespace Database\Seeders;

use App\Models\Project;
use App\Models\ProjectArticle;
use App\Models\ProjectUser;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProjectsSeeder extends Seeder
{
    public function run()
    {
        DB::table('project_has_content')->truncate();
        Project::truncate();
        ProjectUser::truncate();
        ProjectArticle::truncate();

        Project::factory()->count(5)
            ->has(ProjectUser::factory()->count(3))
            ->has(ProjectArticle::factory()->count(3))
            ->create();
    }
}
